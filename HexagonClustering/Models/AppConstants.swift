//
//  AppConstants.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 5.03.23.
//

import Foundation

enum AppConstants {
    static let hexResolution: Int32 = 7
    static let hexResolutionM: Int32 = 6
    static let hexResolutionL: Int32 = 5
}
