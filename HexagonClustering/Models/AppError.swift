//
//  AppError.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 23.02.23.
//

import Foundation

enum AppError: Error {
    case sourceFileNotFound
    case badInputToUncompact
}

