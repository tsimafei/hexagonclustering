//
//  Annotation.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 23.02.23.
//

import Foundation
import CoreLocation

struct Annotation {
    let id: Int
    let coordinate: CLLocationCoordinate2D
    let index: H3Index

    init(id: Int, coordinate: CLLocationCoordinate2D) {
        self.id = id
        self.coordinate = coordinate

        self.index = coordinate.toCellIndex(resolution: AppConstants.hexResolution)
    }
}


