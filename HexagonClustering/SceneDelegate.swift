//
//  SceneDelegate.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 23.02.23.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(
        _ scene: UIScene,
        willConnectTo session: UISceneSession,
        options connectionOptions: UIScene.ConnectionOptions
    ) {
        setupMapView()
    }

    func sceneDidDisconnect(_ scene: UIScene) { }

    func sceneDidBecomeActive(_ scene: UIScene) { }

    func sceneWillResignActive(_ scene: UIScene) { }

    func sceneWillEnterForeground(_ scene: UIScene) { }

    func sceneDidEnterBackground(_ scene: UIScene) { }

    func setupMapView() {
        guard let windowScene = UIApplication.shared.connectedScenes.first as? UIWindowScene else {
            return
        }
        window = UIWindow(windowScene: windowScene)

        guard let window = window else { return }

        let useCase = AnnotationsUseCaseImpl()
        let config = MapConfigModel(annotationsUseCase: useCase)
        let assembly = MapSceneAssembly(config: config)
        
        window.rootViewController = assembly.controller
        window.makeKeyAndVisible()
    }
}

