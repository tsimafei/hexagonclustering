//
//  GeoLoop+Zero.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 5.03.23.
//

import Foundation

extension GeoLoop {
    static var zero: GeoLoop {
        return GeoLoop(numVerts: .zero, verts: nil)
    }
}
