//
//  CLLocationCoordinate2D+H3Index.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 5.03.23.
//

import Foundation
import MapKit

// MARK: - CLLocationCoordinate2D

extension CLLocationCoordinate2D {
    func toCellIndex(resolution: Int32) -> UInt64 {
        var location = toLatLng()
        var outIndex = H3Index.zero
        latLngToCell(&location, resolution, &outIndex)
        return outIndex
    }
    
    func toLatLng() -> LatLng {
        let lat = degsToRads(latitude)
        let lng = degsToRads(longitude)
        return LatLng(lat: lat, lng: lng)
    }
}
