//
//  CGFloat+Half.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 7.03.23.
//

import Foundation

extension Double {
    func half() -> Double {
        self / 2
    }
}
