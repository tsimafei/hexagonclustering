//
//  CellBoundary+Zero.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 5.03.23.
//

import Foundation

public extension CellBoundary {
    static var zero: CellBoundary {
        return CellBoundary(numVerts: .zero, verts: (
            LatLng.zero,
            LatLng.zero,
            LatLng.zero,
            LatLng.zero,
            LatLng.zero,
            LatLng.zero,
            LatLng.zero,
            LatLng.zero,
            LatLng.zero,
            LatLng.zero
        ))
    }
}
