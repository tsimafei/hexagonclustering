//
//  Array+H3Index.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 7.03.23.
//

import Foundation

extension Array where Element == H3Index {
    mutating func toMultiplePolygon() -> Array<Array<Array<LatLng>>> {
        let out: UnsafeMutablePointer<LinkedGeoPolygon> = UnsafeMutablePointer.allocate(capacity: 1)
        let arr: Array<H3Index> = self
        arr.withUnsafeBufferPointer { (inPtr) -> Void in
            cellsToLinkedMultiPolygon(inPtr.baseAddress, Int32(self.count), out)
        }
        var result: Array<Array<Array<LatLng>>> = Array<Array<Array<LatLng>>>()
        var currentPoly: LinkedGeoPolygon? = out.pointee
        while currentPoly != nil {
            var resultLoops: Array<Array<LatLng>> = Array<Array<LatLng>>()
            if currentPoly?.first != nil {
                var currentLoop: LinkedGeoLoop? = currentPoly?.first?.pointee
                guard let currLoop = currentLoop else {
                    break
                }
                var resultLoop: Array<LatLng> = Array<LatLng>()
                var coord: LinkedLatLng? = currLoop.first?.pointee
                while coord != nil {
                    let LatLng = LatLng(lat: coord!.vertex.lat, lng: coord!.vertex.lng)
                    resultLoop.append(LatLng)
                    coord = coord?.next?.pointee
                }
                resultLoops.append(resultLoop)
                currentLoop = currLoop.next?.pointee
            }
            result.append(resultLoops)
            currentPoly = currentPoly?.next?.pointee
        }
        destroyLinkedMultiPolygon(out)
        return result
    }
}

