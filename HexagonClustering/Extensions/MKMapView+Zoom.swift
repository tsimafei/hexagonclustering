//
//  MKMapView+Zoom.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 24.02.23.
//

import Foundation
import MapKit

// MARK: - Constants

private struct Constants {
    static let maxZoom: Double = 24
}

// MARK: - MKMapView

extension MKMapView {
    var zoomLevel: CGFloat {
        return zoomLevel(from: bottomLongitudeDelta)
    }
    
    var visibleCoordinatesPolygon: [CLLocationCoordinate2D] {
        [
            MKMapPoint(x: visibleMapRect.minX, y: visibleMapRect.minY).coordinate,
            MKMapPoint(x: visibleMapRect.maxX, y: visibleMapRect.minY).coordinate,
            MKMapPoint(x: visibleMapRect.maxX, y: visibleMapRect.maxY).coordinate,
            MKMapPoint(x: visibleMapRect.minX, y: visibleMapRect.maxY).coordinate,
            MKMapPoint(x: visibleMapRect.minX, y: visibleMapRect.minY).coordinate
        ]
    }
    
    private var bottomLongitudeDelta: CLLocationDegrees {
        let bottomCoordinates = self.bottomCoordinatesAtPrimeMeridian
        let bottomLongitudeDeltaHorizontalComponent = bottomCoordinates.southEast.longitude - bottomCoordinates.northWest.longitude
        let bottomLongitudeDelta = bottomLongitudeDeltaHorizontalComponent / cos(positiveHeading)
        return bottomLongitudeDelta
    }
    
    private func zoomLevel(from longitudeDelta: CLLocationDegrees) -> CGFloat {
        return log2(360 * self.frame.size.width / (128 * CGFloat(longitudeDelta)))
    }
    
    private var positiveHeading: CLLocationDirection {
        let bottomCoordinates = self.bottomCoordinatesAtPrimeMeridian
        
        let p1 = MKMapPoint(bottomCoordinates.northWest)
        let p2 = MKMapPoint(bottomCoordinates.southEast)
        
        let width = p2.x - p1.x
        let height = p2.y - p1.y
        let hypotenuse = hypot(width, height)
        
        let heading = asin(height / hypotenuse)
        
        return heading
    }
    
    private var bottomCoordinatesAtPrimeMeridian: (northWest: CLLocationCoordinate2D, southEast: CLLocationCoordinate2D) {
        let bottomRect = CGRect(x: .zero, y: bounds.height, width: bounds.width, height: .zero)

        var region = self.convert(bottomRect, toRegionFrom: self)
        
        region.center.longitude = .zero
        
        let northWestCoordinate = CLLocationCoordinate2D(
            latitude: region.center.latitude + (region.span.latitudeDelta.half()),
            longitude: region.center.longitude - (region.span.longitudeDelta.half())
        )
        let southEastCoordinate = CLLocationCoordinate2D(
            latitude: region.center.latitude - (region.span.latitudeDelta.half()),
            longitude: region.center.longitude + (region.span.longitudeDelta.half())
        )
        
        return (northWestCoordinate, southEastCoordinate)
    }
}

