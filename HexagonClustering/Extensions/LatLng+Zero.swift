//
//  LatLng+Zero.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 5.03.23.
//

import Foundation

extension LatLng {
    static var zero: LatLng {
        return LatLng(lat: .zero, lng: .zero)
    }
}
