//
//  H3Index+Boundary.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 5.03.23.
//

import Foundation

extension H3Index {
    func geoBoundary() -> Array<LatLng> {
        var cb: CellBoundary = CellBoundary.zero
        withUnsafeMutablePointer(to: &cb) { (ptr) -> Void in
            cellToBoundary(self, ptr)
        }
        let coords: [LatLng] = Mirror(reflecting: cb.verts).children.compactMap { $0.value as? LatLng }
        return coords[.zero..<Int(cb.numVerts)].map { $0 }
    }
    
    func getLatLng() -> LatLng {
        var latLng: LatLng = LatLng.zero
        withUnsafeMutablePointer(to: &latLng) { (ptr) -> Void in
            cellToLatLng(self, ptr)
        }
        return latLng
    }
}
