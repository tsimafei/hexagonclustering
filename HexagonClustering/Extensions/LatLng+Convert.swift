//
//  LatLng+Convert.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 5.03.23.
//

import Foundation
import MapKit

extension LatLng {
    func toCLLocationCoordinate() -> CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: radsToDegs(self.lat), longitude: radsToDegs(self.lng))
    }
}
