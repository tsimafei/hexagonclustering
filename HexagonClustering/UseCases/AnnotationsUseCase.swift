//
//  AnnotationsUseCase.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 23.02.23.
//

import Foundation
import CoreLocation
import MapKit

// MARK: - Constants

private enum Constants {
    static let fileURL = Bundle.main.url(forResource: "hotspots", withExtension: "csv")

    static let indexesCount = 4
    static let idIndex = 1
    static let latitudeIndex = 2
    static let longitudeIndex = 3
    
    static let zoomLevelVisible: Double = 8
}

// MARK: - AnnotationsUseCase

protocol AnnotationsUseCase {
    typealias Index = (coordinates: [CLLocationCoordinate2D], count: Int, index: H3Index?)
    
    func fetch(completion: @escaping (Result<Void, Error>) -> ())
    func getAnnotations(
        visibleCoordinatesPolygon: [CLLocationCoordinate2D],
        zoomLevel: Double,
        completion: @escaping ((_ indexes: Array<Index>, _ isMultiplePolygon: Bool) -> ())
    )
}

// MARK: - AnnotationsUseCaseImpl

class AnnotationsUseCaseImpl {
    private var collection: [H3Index: [Annotation]] = [:]

    private var getAnnotationsWorkItem: DispatchWorkItem?

    init() {}
}

// MARK: - Fetching

extension AnnotationsUseCaseImpl: AnnotationsUseCase {
    func fetch(completion: @escaping (Result<Void, Error>) -> ()) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            let data: String
            do {
                guard let url = Constants.fileURL else { return }
                data = try String(contentsOf: url)
            } catch {
                DispatchQueue.main.async {
                    completion(.failure(AppError.sourceFileNotFound))
                }
                return
            }

            data.components(separatedBy: "\n")
                .forEach { row in
                    let components = row.components(separatedBy: ",")
                    guard
                        Constants.indexesCount == components.count,
                        let id = Int(components[Constants.idIndex]),
                        let latitude = Double(components[Constants.latitudeIndex]),
                        let longitude = Double(components[Constants.longitudeIndex])
                    else { return }
                    
                    let annotation = Annotation(
                        id: id,
                        coordinate: CLLocationCoordinate2D(
                            latitude: latitude,
                            longitude: longitude
                        )
                    )
                    if self?.collection[annotation.index] != nil {
                        self?.collection[annotation.index]? += [annotation]
                    } else {
                        self?.collection[annotation.index] = [annotation]
                    }
                }
            
            DispatchQueue.main.async {
                completion(.success(()))
            }
        }
    }
}

// MARK: - Getting

extension AnnotationsUseCaseImpl {
    func getAnnotations(
        visibleCoordinatesPolygon: [CLLocationCoordinate2D],
        zoomLevel: Double,
        completion: @escaping ((_ indexes: Array<Index>, _ isMultiplePolygon: Bool) -> ())
    ) {
        guard !collection.isEmpty else { return }
        
        getAnnotationsWorkItem?.cancel()
        getAnnotationsWorkItem = nil
        
        let workItem = DispatchWorkItem(qos: .background) { [weak self] in
            guard let self = self else { return }
        
            var verts = visibleCoordinatesPolygon.compactMap { $0.toLatLng() }

            var unsafeLatLngPtr: UnsafeMutableBufferPointer<LatLng>!
            verts.withUnsafeMutableBufferPointer { unsafeLatLngPtr = $0 }

            let geoLoop = GeoLoop(numVerts: Int32(verts.count), verts: unsafeLatLngPtr.baseAddress)
            var geoPolygon = GeoPolygon(geoloop: geoLoop, numHoles: .zero, holes: nil)

            var maxPoly: Int64 = .zero
            maxPolygonToCellsSize(&geoPolygon, Int32(AppConstants.hexResolution), .zero, &maxPoly)

            var buffer: Array<H3Index> = Array(repeating: .zero, count: Int(maxPoly))
            polygonToCells(&geoPolygon, Int32(AppConstants.hexResolution), .zero, &buffer)
            
            var items = [Index]()
            var isMultiplePolygon = false

            if zoomLevel < Constants.zoomLevelVisible {
                var filteredBuffer = buffer.filter { self.collection[$0] != nil }
                items = filteredBuffer.toMultiplePolygon().compactMap {
                    let coordinates = $0.reduce([], +).compactMap { $0.toCLLocationCoordinate() }
                    return Index(coordinates: coordinates, count: .zero, index: nil)
                }
                isMultiplePolygon = true
            } else {
                items = buffer.compactMap { buffItem in
                    guard let item = self.collection[buffItem] else { return nil }
                    let coordinates = buffItem.geoBoundary().compactMap { $0.toCLLocationCoordinate() }
                    return Index(coordinates: coordinates, count: item.count, index: buffItem)
                }
            }
                                    
            DispatchQueue.global(qos: .background).async {
                completion(items, isMultiplePolygon)
            }
        }
        
        DispatchQueue.global(qos: .userInitiated).async(execute: workItem)
        
        getAnnotationsWorkItem = workItem
    }
}
