//
//  BaseViewCModel.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 6.03.23.
//

import Foundation
import MapKit

// MARK: - Constants

private enum Constants {
    static let zoomLevelVisibleTitle: Double = 11
}

// MARK: - MapViewModelProtocol

protocol MapViewModelProtocol {
    func setup(controller: MapViewControllerProtocol, view: MapViewProtocol)
    func didSetupUI()
    func setRegionVisible(_ visibleCoordinatesPolygon: [CLLocationCoordinate2D], zoomLevel: Double)
    func regionDidChange()
}

// MARK: - MapViewModel

class MapViewModel {
    private let config: MapConfigModel

    private weak var view: MapViewProtocol!
    private weak var controller: MapViewControllerProtocol!

    private var visibleCoordinatesPolygon: [CLLocationCoordinate2D] = []
    private var zoomLevel: Double = .zero
    
    init(configModel: MapConfigModel) {
        self.config = configModel
    }
}

// MARK: - MapViewModelProtocol

extension MapViewModel: MapViewModelProtocol {
    func setup(controller: MapViewControllerProtocol, view: MapViewProtocol) {
        self.controller = controller
        self.view = view
    }
    
    func didSetupUI() {
        controller.showProgressHud()
        config.annotationsUseCase.fetch { [weak self] result in
            self?.controller.hideProgressHud()

            switch result {
            case .success(_):
                self?.regionDidChange()

            case .failure(let error):
                self?.controller.showError(with: error.localizedDescription)
            }
        }
    }
    
    func setRegionVisible(_ visibleCoordinatesPolygon: [CLLocationCoordinate2D], zoomLevel: Double) {
        self.visibleCoordinatesPolygon = visibleCoordinatesPolygon
        self.zoomLevel = zoomLevel
    }

    func regionDidChange() {
        config.annotationsUseCase.getAnnotations(
            visibleCoordinatesPolygon: visibleCoordinatesPolygon,
            zoomLevel: self.zoomLevel
        ) { [weak self] indexes, isMultiplePoligon  in
            DispatchQueue.global(qos: .userInitiated).async { [weak self] in
                guard let self = self else { return }

                let overlays: [(polygon: MKPolygon, annotation: MKAnnotation?)] = indexes.enumerated().compactMap { item in
                    let poly = MKPolygon(coordinates: item.element.coordinates, count: item.element.coordinates.count)
                    var annotation: MKPointAnnotation? = nil

                    if self.zoomLevel > Constants.zoomLevelVisibleTitle, let index = item.element.index {
                        annotation = MKPointAnnotation()
                        annotation?.coordinate = index.getLatLng().toCLLocationCoordinate()
                        annotation?.title = String(item.element.count)
                    }
                    return (poly, annotation)
                }

                let annotations = overlays.compactMap { $0.annotation }
                let polygons = overlays.compactMap { $0.polygon }

                DispatchQueue.main.async {
                    if !annotations.isEmpty {
                        self.view.setupAnnotations(annotations)
                    } else {
                        self.view.cleanAllAnnotations()
                    }
                    self.view.setupMultiOverlays(
                        [
                            isMultiplePoligon ? СombinedMultiPolygon(polygons) : SingleMultiPolygon(polygons)
                        ]
                    )
                }
            }
        }
    }
}
