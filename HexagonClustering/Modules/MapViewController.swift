//
//  BaseViewController.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 5.03.23.
//

import Foundation
import UIKit
import SnapKit
import ProgressHUD

// MARK: - MapViewControllerProtocol

protocol MapViewControllerProtocol: AnyObject {
    func showProgressHud()
    func hideProgressHud()
    func showError(with message: String)
}

// MARK: - MapViewController

class MapViewController: UIViewController {
    private let contentView: MapViewProtocol
    private let viewModel: MapViewModelProtocol

    init(contentView: MapView, viewModel: MapViewModel) {
        self.contentView = contentView
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)

        self.setupContentView(contentView)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        setup()
        setupUI()
        setupConstraints()
    }

    private func setup() {
        viewModel.setup(controller: self, view: contentView)
    }
    
    private func setupUI() {
        ProgressHUD.animationType = .circleRotateChase

        contentView.setupUI()
    }

    private func setupConstraints() {
        contentView.setupConstraints()
    }
    
    private func setupContentView(_ contentView: MapView) {
        view.addSubview(contentView)
        contentView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
}

// MARK: - MapViewControllerProtocol

extension MapViewController: MapViewControllerProtocol {
    func showProgressHud() {
        ProgressHUD.show()
    }

    func hideProgressHud() {
        ProgressHUD.dismiss()
    }

    func showError(with message: String) {
        let alert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
