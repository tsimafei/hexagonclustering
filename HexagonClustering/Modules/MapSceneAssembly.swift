//
//  MapSceneAssembly.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 6.03.23.
//

import Foundation

// MARK: - MapSceneAssembly

class MapSceneAssembly {
    let controller: MapViewController
        
    required init(config: MapConfigModel) {
        let viewModel = MapViewModel(configModel: config)
        let view = MapView(viewModel: viewModel)

        self.controller = MapViewController(contentView: view, viewModel: viewModel)
        viewModel.setup(controller: controller, view: view)
    }
}
