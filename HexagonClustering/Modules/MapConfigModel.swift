//
//  MapConfigModel.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 6.03.23.
//

import Foundation

// MARK: - MapConfigModel

class MapConfigModel {
    let annotationsUseCase: AnnotationsUseCase

    init(annotationsUseCase: AnnotationsUseCase) {
        self.annotationsUseCase = annotationsUseCase
    }
}
