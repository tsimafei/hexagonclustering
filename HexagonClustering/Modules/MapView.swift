//
//  MapView.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 6.03.23.
//

import Foundation
import UIKit
import MapKit

// MARK: - Constants

private enum Constants {
    static let minCenterCoordinateDistance: CLLocationDistance = 10000
    static let maxCenterCoordinateDistance: CLLocationDistance = 2000000
    
    static let maxHilightedZoomLevel: Double = 9
    
    static let fillColorStartMultiple: Double = 0.8
    static let strokeColorStartMultiple: Double = 0.5
}

// MARK: - MapViewProtocol

protocol MapViewProtocol: AnyObject {
    func setupUI()
    func setupConstraints()
    func setupMultiOverlays(_ overlays: [MKMultiPolygon])
    func setupAnnotations(_ annotations: [MKAnnotation])
    func cleanAllAnnotations()
}

// MARK: - MapView

class MapView: UIView {
    private let viewModel: MapViewModelProtocol

    private var mapView = MKMapView(frame: .zero)
    
    private var overlays = [MKOverlay]()
    private var annotations = [MKAnnotation]()

    init(viewModel: MapViewModel) {
        self.viewModel = viewModel
        
        super.init(frame: .zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - MapViewProtocol

extension MapView: MapViewProtocol {
    func setupUI() {
        backgroundColor = .white
        
        addSubview(mapView)
        mapView.delegate = self
        
        mapView.cameraZoomRange = MKMapView.CameraZoomRange(
            minCenterCoordinateDistance: Constants.minCenterCoordinateDistance,
            maxCenterCoordinateDistance: Constants.maxCenterCoordinateDistance
        )
        
        viewModel.didSetupUI()
        viewModel.setRegionVisible(mapView.visibleCoordinatesPolygon, zoomLevel: mapView.zoomLevel)
        
        mapView.register(
            MapAnnotationView.self,
            forAnnotationViewWithReuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier
        )
    }

    func setupConstraints() {
        mapView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func setupMultiOverlays(_ overlays: [MKMultiPolygon]) {
        self.overlays = mapView.overlays
        mapView.addOverlays(overlays)
    }

    func setupAnnotations(_ annotations: [MKAnnotation]) {
        self.annotations = mapView.annotations
        mapView.addAnnotations(annotations)
    }
    
    func cleanAllAnnotations() {
        self.mapView.removeAnnotations(self.mapView.annotations)
    }
}

// MARK: - MapViewProtocol

extension MapView: MKMapViewDelegate {
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        viewModel.setRegionVisible(mapView.visibleCoordinatesPolygon, zoomLevel: mapView.zoomLevel)
        viewModel.regionDidChange()
    }

    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let diffZoomLevel = max(.zero, 1 - mapView.zoomLevel / Constants.maxHilightedZoomLevel)

        let renderer = MKMultiPolygonRenderer(overlay: overlay)

        if let overlay = overlay as? AppMultiPolygon {
            renderer.fillColor = overlay.fillColor.withAlphaComponent(min(1, Constants.fillColorStartMultiple + diffZoomLevel))
            renderer.strokeColor = overlay.strokeColor.withAlphaComponent(min(1, Constants.strokeColorStartMultiple + diffZoomLevel))
        }

        return renderer
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        MapAnnotationView(annotation: annotation, reuseIdentifier: MKMapViewDefaultAnnotationViewReuseIdentifier)
    }

    func mapView(_ mapView: MKMapView, didAdd renderers: [MKOverlayRenderer]) {
        self.mapView.removeOverlays(self.overlays)
        self.mapView.removeAnnotations(self.annotations)
        
        self.annotations = []
        self.overlays = []
    }
}
