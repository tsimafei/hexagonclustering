//
//  AppMultiPolygon.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 7.03.23.
//

import UIKit

protocol AppMultiPolygon {
    var fillColor: UIColor { get }
    var strokeColor: UIColor { get }
}
