//
//  SingleMultiPolygon.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 7.03.23.
//

import Foundation
import MapKit

// MARK: - Constants

private enum Constants {
    static let fillColor = UIColor.gray
    static let strokeColor = UIColor.blue
}

// MARK: - Constants

class SingleMultiPolygon: MKMultiPolygon, AppMultiPolygon {
    let fillColor = Constants.fillColor
    let strokeColor = Constants.strokeColor
}
