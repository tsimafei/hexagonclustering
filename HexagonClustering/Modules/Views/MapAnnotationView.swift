//
//  MapAnnotation.swift
//  HexagonClustering
//
//  Created by Tim Harhun on 7.03.23.
//

import Foundation
import MapKit

// MARK: - Constants

private enum Constants {
    static let frame = CGRect(x: .zero, y: .zero, width: 50, height: 50)
    static let font = UIFont.boldSystemFont(ofSize: 12)
}

// MARK: - MapAnnotationView

class MapAnnotationView: MKAnnotationView {
    let countLabel = UILabel(frame: .zero)
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        frame = Constants.frame
        
        canShowCallout = true
        setupUI()
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Setup
    
    private func setupUI() {
        backgroundColor = .clear
        
        addSubview(countLabel)
        countLabel.frame = bounds
        
        countLabel.text = annotation?.title ?? ""
        
        countLabel.textAlignment = .center
        countLabel.font = Constants.font
        countLabel.textColor = .white
    }
}
